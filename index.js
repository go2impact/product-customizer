const express = require('express');
const app = express();
const fs = require('fs');
const bodyParser = require('body-parser');
const path = require("path");
app.set('view engine', 'ejs');


app.use(express.static('public'));

app.get("/pixie", function(req,res) {

    res.sendFile(path.join(__dirname+'/index.html'));
});

app.listen(9000, function() {
    console.log("Listening on port 9000")
});