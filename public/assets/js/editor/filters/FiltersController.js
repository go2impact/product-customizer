angular.module('image.filters', [])

    .controller('FiltersController', ['$scope', '$rootScope', 'canvas', 'filters', function ($scope, $rootScope, canvas, filters) {
        $scope.$watch('filterOptions', function () {
            if ($scope.filterOptions) {
                var filteredFilters = filters.all;

                if (!$scope.filterOptions.showGrayScale) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('grayscale'), 1);
                }

                if (!$scope.filterOptions.showInvert) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('invert'), 1);
                }

                if (!$scope.filterOptions.showSepia) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('sepia'), 1);
                }

                if (!$scope.filterOptions.showSepia2) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('sepia2'), 1);
                }

                if (!$scope.filterOptions.showRemoveWhite) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('removeWhite'), 1);
                }

                if (!$scope.filterOptions.showBrightness) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('brightness'), 1);
                }

                if (!$scope.filterOptions.showNoise) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('noise'), 1);
                }

                if (!$scope.filterOptions.showGradient) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('GradientTransparency'), 1);
                }

                if (!$scope.filterOptions.showPixelate) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('pixelate'), 1);
                }

                if (!$scope.filterOptions.showSharpen) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('sharpen'), 1);
                }

                if (!$scope.filterOptions.showBlur) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('blur'), 1);
                }

                if (!$scope.filterOptions.showEmboss) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('emboss'), 1);
                }

                if (!$scope.filterOptions.showTint) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('tint'), 1);
                }

                if (!$scope.filterOptions.showMultiply) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('multiply'), 1);
                }

                if (!$scope.filterOptions.showBlend) {
                    filteredFilters.splice(filteredFilters.map(function (item) {
                        return item.name;
                    }).indexOf('blend'), 1);
                }


                filters.all = filteredFilters;
                $scope.filters = filters;
            }
        });


        $scope.filters = filters;

        $rootScope.$on('history.loaded', function () {
            if (!canvas.mainImage) return;

            var appliedFilters = [];

            for (var i = 0; i < canvas.mainImage.filters.length; i++) {
                appliedFilters.push(canvas.mainImage.filters[i].name);
            }

            $scope.$apply(function () {
                filters.appliedFilters = appliedFilters;
            });
        });
    }]);
