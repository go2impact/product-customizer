angular.module('image.shapes', [])

    .controller('SimpleShapesController', ['$scope', '$rootScope', '$timeout', 'canvas', 'simpleShapes', function ($scope, $rootScope, $timeout, canvas, simpleShapes) {
        $scope.shapes = simpleShapes;

        $scope.available = ['rect', 'triangle', 'circle', 'ellipse', 'polygon'];

        $scope.$watch('shapeOptions', function () {
            var shapesAvailable = simpleShapes.available;
            if ($scope.shapeOptions) {
                if (!$scope.shapeOptions.showCircle) {
                    shapesAvailable.splice(shapesAvailable.map(function (item) {
                        return item.name;
                    }).indexOf('circle'), 1);
                }

                if (!$scope.shapeOptions.showRectangle) {
                    shapesAvailable.splice(shapesAvailable.map(function (item) {
                        return item.name;
                    }).indexOf('rect'), 1);
                }

                if (!$scope.shapeOptions.showTriangle) {
                    shapesAvailable.splice(shapesAvailable.map(function (item) {
                        return item.name;
                    }).indexOf('triangle'), 1);
                }

                if (!$scope.shapeOptions.showEllipse) {
                    shapesAvailable.splice(shapesAvailable.map(function (item) {
                        return item.name;
                    }).indexOf('ellipse'), 1);
                }

                if (!$scope.shapeOptions.showPolygon) {
                    shapesAvailable.splice(shapesAvailable.map(function (item) {
                        return item.name;
                    }).indexOf('polygon'), 1);
                }
            }

            simpleShapes.available = shapesAvailable;
            $scope.shapes = simpleShapes;
        });

        $scope.isPanelEnabled = function () {
            var obj = canvas.fabric.getActiveObject();
            return obj && $scope.available.indexOf(obj.name) > -1 && simpleShapes.selected.options;
        };

        canvas.fabric.on('object:selected', function (object) {
            if ($rootScope.activeTab !== 'simple-shapes') return;
            simpleShapes.selectShape(object.target.name);
        });
    }]);