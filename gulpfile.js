// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var less   = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var path = require('path');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('less', function () {
  gulp.src('./public/assets/less/main.less')
    .pipe(less())
    .pipe(gulp.dest('./public/assets/css'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('integrate-less', function () {
    gulp.src('./public/assets/less/integrate.less')
        .pipe(less())
        .pipe(gulp.dest('./public/assets/css'))
        .pipe(browserSync.reload({stream:true}));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src([
        'public/assets/js/editor/resources/colors.js',
        'public/assets/js/editor/resources/gradients.js',
    	'public/assets/js/vendor/jquery.js',
        'public/assets/js/vendor/jquery-ui.js',
        'public/assets/js/vendor/file-saver.js',
        'public/assets/js/vendor/pagination.js',
        'public/assets/js/vendor/spectrum.js',
        'public/assets/js/vendor/hammer.js',
        'public/assets/js/vendor/scrollbar.js',
    	'public/assets/js/vendor/angular.min.js',
        'public/assets/js/vendor/angular-animate.js',
        'public/assets/js/vendor/angular-aria.js',
        'public/assets/js/vendor/angular-material.js',
        'public/assets/js/vendor/angular-sortable.js',
    	'public/assets/js/vendor/fabric.js',
    	'public/assets/js/editor/App.js',
        'public/assets/js/editor/LocalStorage.js',
        'public/assets/js/editor/Settings.js',
        'public/assets/js/editor/Keybinds.js',
        'public/assets/js/editor/Canvas.js',
        'public/assets/js/editor/crop/cropper.js',
        'public/assets/js/editor/crop/cropzone.js',
        'public/assets/js/editor/crop/cropController.js',
        'public/assets/js/editor/basics/RotateController.js',
        'public/assets/js/editor/basics/CanvasBackgroundController.js',
        'public/assets/js/editor/basics/ResizeController.js',
        'public/assets/js/editor/basics/RoundedCornersController.js',
        'public/assets/js/editor/zoomController.js',
        'public/assets/js/editor/TopPanelController.js',
        'public/assets/js/editor/directives/Tabs.js',
        'public/assets/js/editor/directives/PrettyScrollbar.js',
        'public/assets/js/editor/directives/ColorPicker.js',
        'public/assets/js/editor/directives/FileUploader.js',
        'public/assets/js/editor/directives/TogglePanelVisibility.js',
        'public/assets/js/editor/directives/ToggleSidebar.js',
        'public/assets/js/editor/text/Text.js',
        'public/assets/js/editor/text/TextController.js',
        'public/assets/js/editor/text/TextAlignButtons.js',
        'public/assets/js/editor/text/TextDecorationButtons.js',
        'public/assets/js/editor/text/Fonts.js',
        'public/assets/js/editor/drawing/Drawing.js',
        'public/assets/js/editor/drawing/DrawingController.js',
        'public/assets/js/editor/drawing/RenderBrushesDirective.js',
        'public/assets/js/editor/History.js',
        'public/assets/js/editor/Saver.js',
        'public/assets/js/editor/filters/FiltersController.js',
        'public/assets/js/editor/filters/Filters.js',
        'public/assets/js/editor/shapes/SimpleShapesController.js',
        'public/assets/js/editor/shapes/StickersController.js',
        'public/assets/js/editor/shapes/StickersCategories.js',
        'public/assets/js/editor/shapes/SimpleShapes.js',
        'public/assets/js/editor/shapes/Polygon.js',
        'public/assets/js/editor/objects/ObjectsPanelController.js',
	])
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('public/assets/js'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('minify', function() {
	gulp.src('public/assets/js/scripts.min.js').pipe(uglify()).pipe(gulp.dest('public/assets/js'));

	gulp.src('public/assets/css/main.css')
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false,
		remove: false
	}))
	.pipe(minifyCSS({compatibility: 'ie10'}))
	.pipe(gulp.dest('public/assets/css'));

	gulp.src('public/assets/css/integrate.css')
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false,
		remove: false
	}))
	.pipe(minifyCSS({compatibility: 'ie10'}))
	.pipe(gulp.dest('public/assets/css'))
});

// Watch Files For Changes
gulp.task('watch', function() {
	browserSync({
        proxy: "http://localhost:9000/pixie"
    });

    gulp.watch('public/assets/js/**/*.js', ['scripts']);
    gulp.watch('public/assets/less/**/*.less', ['less']);
    gulp.watch('public/assets/less/**/integrate.less', ['integrate-less']);
});

// Default Task
gulp.task('default', ['scripts']);